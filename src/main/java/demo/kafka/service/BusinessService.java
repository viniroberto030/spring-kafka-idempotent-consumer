package demo.kafka.service;

import java.util.List;
import java.util.UUID;

import demo.kafka.domain.BusinessEntity;
import demo.kafka.domain.OutboxEntity;
import demo.kafka.event.RequestEvent;
import demo.kafka.exception.KafkaDemoException;
import demo.kafka.exception.KafkaDemoRetryableException;
import demo.kafka.properties.KafkaDemoProperties;
import demo.kafka.repository.BusinessRepository;
import demo.kafka.txmanager.TxManagerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
@RequiredArgsConstructor
public class BusinessService extends TxManagerService {

    @Autowired
    private KafkaDemoProperties properties;

    @Autowired
    private BusinessRepository businessRepository;

    public List<OutboxEntity> executeBusiness(String eventId, String key, RequestEvent event) {
        callThirdparty(key);

        businessRepository.save(BusinessEntity.builder()
                        .campo("Novo campo")
                        .id(UUID.fromString(eventId))
                .build());

        return List.of(
                OutboxEntity.builder()
                        .id(UUID.fromString(eventId))
                        .version("v1")
                        .payload(event.getData())
                        .destination(event.getTopicoSucesso())
                        .timestamp(System.currentTimeMillis())
                        .build()
        );
    }

    private void callThirdparty(String key) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            ResponseEntity<String> response = restTemplate.getForEntity(properties.getThirdpartyEndpoint() + "/" + key, String.class);
            if (response.getStatusCode().is2xxSuccessful()) {
                return;
            }
            throw new RuntimeException("error " + response.getStatusCodeValue());
        } catch (HttpServerErrorException e) {
            throw new KafkaDemoRetryableException(e);
        } catch (ResourceAccessException e) {
            throw new KafkaDemoRetryableException(e);
        } catch (Exception e) {
            throw new KafkaDemoException(e);
        }
    }

    @Override
    public List<OutboxEntity> processarRegraNegocio(String eventId, String key, RequestEvent request) {
        return executeBusiness(eventId, key, request);
    }

    @Override
    public String gerarChaveUnicaRequest(RequestEvent request) {
        return request.getData();
    }
}
