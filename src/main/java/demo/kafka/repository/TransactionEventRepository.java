package demo.kafka.repository;

import java.util.UUID;

import demo.kafka.domain.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface  TransactionEventRepository extends JpaRepository<TransactionEntity, UUID> {

    @Modifying
    @Query(value = "INSERT INTO TB_TRANSACTION (id, checksum) VALUES (:id, :checksum) ON CONFLICT (id) DO NOTHING", nativeQuery = true)
    void saveIgnoreConflict(@Param("id") UUID id, @Param("checksum") long checksum);
}
