package demo.kafka.consumer;

import java.util.List;

import demo.kafka.domain.OutboxEntity;
import demo.kafka.event.RequestEvent;
import demo.kafka.lib.KafkaClient;
import demo.kafka.mapper.JsonMapper;
import demo.kafka.service.BusinessService;
import demo.kafka.txmanager.TxManagerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@RequiredArgsConstructor
@Component
public class KafkaIdempotentConsumerWithOutbox {
    private final BusinessService businessService;

    private static final String TOPICO_SUCESSO = "demo-outbound-topic";
    private static final String TOPICO_ERRO = "demo-outbound-conflict-topic";

    @KafkaListener(
            topics = "demo-idempotent-with-outbox-inbound-topic",
            groupId = "kafkaConsumerGroup",
            containerFactory = "kafkaListenerContainerFactory"
    )
    public void listen(
            @Header(KafkaClient.EVENT_ID_HEADER_KEY) String eventId,
            @Header(KafkaHeaders.RECEIVED_KEY) String key,
            @Payload final String payload,
            Acknowledgment acknowledgment
    ) {
        try{
            RequestEvent event = JsonMapper.readFromJson(payload, RequestEvent.class);
            event.setTopicoErro(TOPICO_ERRO);
            event.setTopicoSucesso(TOPICO_SUCESSO);
            businessService.processarTransacao(eventId, key, event);
        }catch (UnexpectedRollbackException e){
            log.info("### UnexpectedRollbackException... Acredito que dá para tratar com o  ON CONFLICT (id) DO NOTHING do Postgres");
        }

        acknowledgment.acknowledge();
    }
}
