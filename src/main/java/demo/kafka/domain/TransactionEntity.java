package demo.kafka.domain;

import java.io.Serializable;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Transient;
import org.springframework.data.domain.Persistable;

@Entity(name="TB_TRANSACTION")
public class TransactionEntity implements Serializable, Persistable<UUID> {

    @Id
    @Column(name="id")
    private UUID id;

    @Column(name="checksum")
    private long checksum;

    public TransactionEntity(){}

    public TransactionEntity(final UUID id, long checksum) {
        this.id = id;
        this.checksum = checksum;
    }

    @Transient
    @Override
    public UUID getId() {
        return id;
    }

    public long getChecksum(){
        return checksum;
    }

    /**
     * Ensures Hibernate always does an INSERT operation when save() is called.
     */
    @Transient
    @Override
    public boolean isNew() {
        return true;
    }
}
