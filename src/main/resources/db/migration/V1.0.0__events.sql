CREATE SCHEMA IF NOT EXISTS kafka_demo_idempotent_consumer;

CREATE TABLE kafka_demo_idempotent_consumer.tb_transaction (
    id uuid NOT NULL,
    checksum varchar(50) NOT NULL,
    CONSTRAINT processed_event_ids_pkey PRIMARY KEY (id)
);

CREATE TABLE kafka_demo_idempotent_consumer.tb_outbox (
                              id uuid NOT NULL,
                              destination varchar(255) NULL,
                              payload varchar(4096) NULL,
                              timestamp int8 NOT NULL,
                              version varchar(255) NULL
);

CREATE TABLE kafka_demo_idempotent_consumer.tb_business (
                              id uuid NOT NULL,
                              campo varchar(255) NULL
);

