package demo.kafka.repository;

import demo.kafka.domain.BusinessEntity;
import demo.kafka.domain.OutboxEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BusinessRepository extends JpaRepository<BusinessEntity, UUID> {
}
