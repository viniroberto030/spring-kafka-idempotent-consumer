package demo.kafka.txmanager;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerInterceptor;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.util.Map;

@Slf4j
public class CustomConsumerInterceptor implements  ConsumerInterceptor<String, String> {
    @Override
    public ConsumerRecords<String, String> onConsume(ConsumerRecords consumerRecords) {
        log.info("### onConsume");
        return consumerRecords;
    }

    @Override
    public void close() {
        log.info("### close");
    }

    @Override
    public void onCommit(Map map) {
        log.info("### onCommit");
    }

    @Override
    public void configure(Map<String, ?> map) {
        log.info("### configure");
    }
}
