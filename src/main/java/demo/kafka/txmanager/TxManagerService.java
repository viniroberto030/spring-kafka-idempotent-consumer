package demo.kafka.txmanager.service;

import demo.kafka.txmanager.context.TxContext;
import demo.kafka.txmanager.domain.OutboxEntity;
import demo.kafka.txmanager.domain.TransactionEntity;
import demo.kafka.txmanager.event.ResponseErro;
import demo.kafka.txmanager.event.TipoProcessamentoEnum;
import demo.kafka.txmanager.event.impl.RequestEventAsync;
import demo.kafka.txmanager.event.impl.ResponseSagaEvent;
import demo.kafka.txmanager.exception.BusinessValidationException;
import demo.kafka.txmanager.exception.TransactionDuplicatedException;
import demo.kafka.txmanager.repository.OutboxEventRepository;
import demo.kafka.txmanager.repository.TransactionEventRepository;
import demo.kafka.txmanager.util.JsonMapper;
import demo.kafka.txmanager.util.KafkaClient;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.util.ObjectUtils;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

@Slf4j
@RequiredArgsConstructor
public abstract class TxManagerService{
    private static final String RETORNO_OK = "retorno-ok";
    private static final String RETORNO_ERRO = "retorno-erro";
    @Autowired
    private TransactionEventRepository transactionEventRepository;

    @Autowired
    private KafkaClient kafkaClient;

    @Autowired
    private OutboxEventRepository outboxEventRepository;

    @Autowired
    private EntityManager entityManager;

    public void processarTransacao(TxContext context) {
        log.info("### INICIO TRANSACATION MANAGER: {} ###", context.getRequest().getIdTransacao());
        long checksum = gerarChecksum(context);
        try{
            this.salvarTransacao(context.getRequest().getIdTransacao(), checksum);
             processarRegraNegocio(context);
            log.info("### Regra de negócio processada com sucesso {} ###", context.getRequest().getIdTransacao());
            this.salvarOutbox(context, RETORNO_OK);
            validarResponseSincrono(context);
            entityManager.flush();
        }catch (TransactionDuplicatedException e){
            TransactionEntity transactionEntity = transactionEventRepository.findById(context.getRequest().getIdTransacao()).get();

            if(transactionEntity.getChecksum() == checksum){
                log.info("### Checksum recuperado é igual ao da nova requisição: {} - {} ###", transactionEntity.getChecksum(), checksum);
                var outboxList = this.outboxEventRepository.findAllById(Collections.singleton(context.getRequest().getIdTransacao()));
                gerarResponse(context, outboxList);
                log.info("### OutboxEvent Recuperados do Banco de Dados: {} ###", context.getResponseEvents().size());
            }else{
                log.info("### Checksum encontrado é diferente ao da nova requisição: {} - {} ###", transactionEntity.getChecksum(), checksum);
                context.setResponse(ResponseErro.builder()
                        .codigoErro("000")
                        .descricaoErro("Id Transação utilizada em outra transação.")
                        .build());
            }
        }catch (BusinessValidationException e){
            log.info("### Validação de negócio não atendidade - {} {}", e.getCodigoErro(), e.getDescricaoErro());
            context.setResponse(ResponseErro.builder()
                            .codigoErro(e.getCodigoErro())
                            .descricaoErro(e.getDescricaoErro())
                            .build());
            this.salvarOutbox(context, RETORNO_ERRO);
        }catch (Exception e){
            log.info("### {}", e.getMessage());
            throw e;
        }

        this.enviarEventosSaga(context);
        this.enviarEventoResponse(context);
        log.info("### FIM TRANSACATION MANAGER: {} ###", context.getRequest().getIdTransacao());
    }

    private void enviarEventosSaga(TxContext context) {
        if(!ObjectUtils.isEmpty(context.getResponseEvents())){
            context.getResponseEvents().forEach(responseEvent ->{
                var sagaEvent = (ResponseSagaEvent) responseEvent;
                enviarEvento(context, sagaEvent);
            });
        }
    }

    private void enviarEventoResponse(TxContext context) {
        if(TipoProcessamentoEnum.ASYNC.equals(context.getTipoProcessamento())
                && !Objects.isNull(context.getResponse())) {
            var sagaEvent = gerarResponseEvent(context);
            enviarEvento(context, sagaEvent);
        }
    }

    private void enviarEvento(TxContext context, ResponseSagaEvent sagaEvent) {
        try {
            this.kafkaClient.sendMessage(sagaEvent);
            log.info("### OutboxEvent Enviado: id = {} topic = {} ###", sagaEvent.getIdTransacao(), sagaEvent.getTopico());
        } catch (Exception e) {
            log.info("### Erro ao enviar OutboxEvent: id = {} ###", context.getRequest().getIdTransacao());
            throw new RuntimeException(e);
        }
    }

    private static void validarResponseSincrono(final TxContext context){
        if(TipoProcessamentoEnum.SYNC.equals(context.getTipoProcessamento())
                && context.getResponse() == null){
            log.info("### Nenhum response para o retorno sincrono encontrado.");
            throw new IllegalStateException("Response síncrono obrigatório!");
        }
    }

    private void gerarResponse(TxContext context, List<OutboxEntity> entityList) {
        context.setResponseEvents((entityList.stream()
                .filter(outboxEntity -> !List.of(RETORNO_ERRO, RETORNO_OK).contains(outboxEntity.getTopico()))
                .map(outboxEntity -> ResponseSagaEvent.builder()
                        .idTransacao(outboxEntity.getIdTransacaoEntrada())
                        .body(outboxEntity.getBody())
                        .topico(outboxEntity.getTopico())
                        .chaveOrdenacao(outboxEntity.getChaveOrdenacao())
                        .build()).collect(Collectors.toList())));

        context.setResponse(entityList.stream()
                .filter(outboxEntity -> List.of(RETORNO_ERRO, RETORNO_OK).contains(outboxEntity.getTopico()))
                .findFirst()
                .map(outboxEntity -> {
                    if(RETORNO_ERRO.equals(outboxEntity.getTopico())){
                        return JsonMapper.readFromJson(outboxEntity.getBody(), ResponseErro.class);
                    }
                    return JsonMapper.readFromJson(outboxEntity.getBody(), context.getClassResponse());
                }).orElse(null));
    }

    private static ResponseSagaEvent gerarResponseEvent(TxContext context) {
        var response = ResponseSagaEvent.builder()
                .idTransacao(context.getRequest().getIdTransacao())
                .body(JsonMapper.writeToJson(context.getResponse()));
        if(context.getRequest() instanceof RequestEventAsync request) {
            response
                .topico(request.getTopicoRespostaErro())
                .chaveOrdenacao(request.getChaveOrdenacao());
        }

        return response.build();
    }

    private void salvarTransacao(String idTransacao, long checksum) {
        try {
            //TODO Implementar o testContainer usando postgres para utilizar o ON CONFLICT DO NOTHING e remover esse select
            transactionEventRepository.findById(idTransacao)
                            .ifPresent(transactionEntity -> {
                                log.info("### Transação já processada: {} ###", idTransacao);
                                throw new TransactionDuplicatedException();
                            });
            transactionEventRepository.saveAndFlush(new TransactionEntity(idTransacao, checksum));
            log.info("### Transação salva com o Id: {} ###", idTransacao);
        } catch (DataIntegrityViolationException e) {
            entityManager.clear();
            log.info("### Transação já processada: {} ###", idTransacao);
            throw new TransactionDuplicatedException();
        }
    }

    private long gerarChecksum(TxContext context){
        byte[] bytes = gerarChaveUnicaRequest(context).getBytes();
        Checksum crc32 = new CRC32();
        crc32.update(bytes, 0, bytes.length);
        return crc32.getValue();
    }

    private void salvarOutbox(TxContext context, String topico) {
        try{
            List<OutboxEntity> lista = (List<OutboxEntity>) context.getResponseEvents().stream()
                    .map(event -> {
                        var sagaEvent = (ResponseSagaEvent) event;
                        return OutboxEntity.builder()
                                    .idTransacaoEntrada(sagaEvent.getIdTransacao())
                                    .body(sagaEvent.getBody())
                                    .chaveOrdenacao(sagaEvent.getChaveOrdenacao())
                                    .topico(sagaEvent.getTopico())
                                    .build();
                    })
                    .collect(Collectors.toList());

            if(context.getResponse() != null){
                var response = OutboxEntity.builder()
                        .idTransacaoEntrada(context.getRequest().getIdTransacao())
                        .body(JsonMapper.writeToJson(context.getResponse()))
                        .topico(topico);
                if(TipoProcessamentoEnum.ASYNC.equals(context.getTipoProcessamento())){
                    var requestAsync = (RequestEventAsync) context.getRequest();
                    var topicoResponse = RETORNO_OK.equals(topico)
                            ? requestAsync.getTopicoRespostaSucesso()
                            : requestAsync.getTopicoRespostaErro();
                    response.topico(topicoResponse);
                }
                lista.add(response.build());
            }

            outboxEventRepository.saveAllAndFlush(lista);
            log.info("### OutboxEvents salvo: {} ###", lista.size());
        }catch (DataIntegrityViolationException e){
            e.printStackTrace();
            throw new RuntimeException("Erro ao salvar Outbox");
        }
    }

    public abstract void processarRegraNegocio(TxContext context);

    public abstract String gerarChaveUnicaRequest(TxContext context);
}
