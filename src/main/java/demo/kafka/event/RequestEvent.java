package demo.kafka.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestEvent {

    private String id;
    private String data;
    private String topicoSucesso;
    private String topicoErro;
}
