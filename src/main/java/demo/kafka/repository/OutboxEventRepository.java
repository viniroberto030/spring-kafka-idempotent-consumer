package demo.kafka.repository;

import java.util.UUID;

import demo.kafka.domain.OutboxEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OutboxEventRepository extends JpaRepository<OutboxEntity, UUID> {
}
