package demo.kafka.util;

import demo.kafka.event.RequestEvent;

public class TestEventData {

    public static String INBOUND_DATA = "event data";
    public static String INBOUND_CONFLICT_DATA = "event data Conflict";

    public static RequestEvent buildDemoInboundEvent(String id) {
        return RequestEvent.builder()
                .id(id)
                .data(INBOUND_DATA)
                .build();
    }

    public static RequestEvent buildDemoInboundConflictEvent(String id) {
        return RequestEvent.builder()
                .id(id)
                .data(INBOUND_CONFLICT_DATA)
                .build();
    }
}
