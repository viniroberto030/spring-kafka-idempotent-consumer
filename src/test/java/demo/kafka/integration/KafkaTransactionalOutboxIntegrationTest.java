package demo.kafka.integration;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import demo.kafka.consumer.KafkaIdempotentConsumerWithOutbox;
import demo.kafka.domain.OutboxEntity;
import demo.kafka.event.RequestEvent;
import demo.kafka.repository.BusinessRepository;
import demo.kafka.repository.OutboxEventRepository;
import demo.kafka.util.TestEventData;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

import static com.github.tomakehurst.wiremock.client.WireMock.exactly;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static demo.kafka.util.TestEventData.buildDemoInboundConflictEvent;
import static demo.kafka.util.TestEventData.buildDemoInboundEvent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@Slf4j
@EmbeddedKafka(controlledShutdown = true, topics = "demo-idempotent-with-outbox-inbound-topic")
public class KafkaTransactionalOutboxIntegrationTest extends IntegrationTestBase {

    final static String DEMO_IDEMPOTENT_AND_OUTBOX_TEST_TOPIC = "demo-idempotent-with-outbox-inbound-topic";

    @Autowired
    private OutboxEventRepository outboxEventRepository;

    @Autowired
    private BusinessRepository businessRepository;

    @Autowired
    private KafkaIdempotentConsumerWithOutbox kafkaIdempotentConsumerWithOutbox;

    @Autowired
    private KafkaTestListener testReceiver;

    @Autowired
    private KafkaTestConflictListener testConflictReceiver;

    @Configuration
    static class TestConfig {

        @Bean
        public KafkaTestListener testReceiver() {
            return new KafkaTestListener();
        }

        @Bean
        public KafkaTestConflictListener testConflictReceiver() {
            return new KafkaTestConflictListener();
        }
    }

    @BeforeEach
    public void setUp() {
        super.setUp();
        outboxEventRepository.deleteAll();
        businessRepository.deleteAll();

        testReceiver.counter.set(0);
        testConflictReceiver.counter.set(0);
    }

    public static class KafkaTestListener {
        AtomicInteger counter = new AtomicInteger(0);

        @KafkaListener(groupId = "KafkaIdempotentConsumerIntegrationTest", topics = "demo-outbound-topic", autoStartup = "true")
        void receive(@Payload final String payload, @Headers final MessageHeaders headers) {
            log.debug("KafkaTestListener - Received message: " + payload);
            counter.incrementAndGet();
        }
    }

    public static class KafkaTestConflictListener {
        AtomicInteger counter = new AtomicInteger(0);

        @KafkaListener(groupId = "KafkaIdempotentConsumerIntegrationTest", topics = "demo-outbound-conflict-topic", autoStartup = "true")
        void receive(@Payload final String payload, @Headers final MessageHeaders headers) {
            log.debug("KafkaTestListener - Received message: " + payload);
            counter.incrementAndGet();
        }
    }

    @Test
    public void testEventDeduplication_TransactionalOutbox() throws Exception {
        String key = UUID.randomUUID().toString();
        String eventId = UUID.randomUUID().toString();
        stubWiremock("/api/kafkaidempotentconsumerdemo/" + key, 200, "Success");

        RequestEvent inboundEvent = buildDemoInboundEvent(key);
        sendMessage(DEMO_IDEMPOTENT_AND_OUTBOX_TEST_TOPIC, eventId, key, inboundEvent);
        sendMessage(DEMO_IDEMPOTENT_AND_OUTBOX_TEST_TOPIC, eventId, key, inboundEvent);
        sendMessage(DEMO_IDEMPOTENT_AND_OUTBOX_TEST_TOPIC, eventId, key, inboundEvent);

        // Check an event has been written to the outbox table.
        Awaitility.await().atMost(10, TimeUnit.SECONDS).pollDelay(100, TimeUnit.MILLISECONDS)
                .until(outboxEventRepository::count, equalTo(1L));

        Awaitility.await().atMost(10, TimeUnit.SECONDS).pollDelay(100, TimeUnit.MILLISECONDS)
                .until(testReceiver.counter::get, equalTo(3));

        // Ensure no duplicate event is written to the outbox table.
        TimeUnit.SECONDS.sleep(5);
        verify(exactly(1), getRequestedFor(urlEqualTo("/api/kafkaidempotentconsumerdemo/" + key)));

        assertThat(testReceiver.counter.get(), equalTo(3));

        assertThat(businessRepository.count(), equalTo(1L));
        assertThat(outboxEventRepository.count(), equalTo(1L));
        OutboxEntity outboxEntity = outboxEventRepository.findAll().get(0);
        assertThat(outboxEntity.getPayload(), equalTo(TestEventData.INBOUND_DATA));
    }

    @Test
    public void testEventRetry_TransactionalOutbox() throws Exception {
        String key = UUID.randomUUID().toString();
        String eventId = UUID.randomUUID().toString();
        stubWiremock("/api/kafkaidempotentconsumerdemo/" + key, 200, "Success");

        RequestEvent inboundEvent = buildDemoInboundEvent(key);
        sendMessage(DEMO_IDEMPOTENT_AND_OUTBOX_TEST_TOPIC, eventId, key, inboundEvent);
        sendMessage(DEMO_IDEMPOTENT_AND_OUTBOX_TEST_TOPIC, eventId, key, inboundEvent);

        // Check an event has been written to the outbox table.
        Awaitility.await().atMost(10, TimeUnit.SECONDS).pollDelay(100, TimeUnit.MILLISECONDS)
                .until(outboxEventRepository::count, equalTo(1L));

        Awaitility.await().atMost(10, TimeUnit.SECONDS).pollDelay(100, TimeUnit.MILLISECONDS)
                .until(testReceiver.counter::get, equalTo(2));

        verify(exactly(1), getRequestedFor(urlEqualTo("/api/kafkaidempotentconsumerdemo/" + key)));

        assertThat(testReceiver.counter.get(), equalTo(2));

        assertThat(businessRepository.count(), equalTo(1L));
        assertThat(outboxEventRepository.count(), equalTo(1L));
        OutboxEntity outboxEntity = outboxEventRepository.findAll().get(0);
        assertThat(outboxEntity.getPayload(), equalTo(TestEventData.INBOUND_DATA));
    }

    @Test
    public void testEventConflict_TransactionalOutbox() throws Exception {
        String key = UUID.randomUUID().toString();
        String eventId = UUID.randomUUID().toString();
        stubWiremock("/api/kafkaidempotentconsumerdemo/" + key, 200, "Success");

        RequestEvent inboundEvent = buildDemoInboundEvent(key);
        sendMessage(DEMO_IDEMPOTENT_AND_OUTBOX_TEST_TOPIC, eventId, key, inboundEvent);
        RequestEvent inboundEventConflict = buildDemoInboundConflictEvent(key);
        sendMessage(DEMO_IDEMPOTENT_AND_OUTBOX_TEST_TOPIC, eventId, key, inboundEventConflict);

        // Check an event has been written to the outbox table.
        Awaitility.await().atMost(10, TimeUnit.SECONDS).pollDelay(100, TimeUnit.MILLISECONDS)
                .until(outboxEventRepository::count, equalTo(1L));

        Awaitility.await().atMost(10, TimeUnit.SECONDS).pollDelay(100, TimeUnit.MILLISECONDS)
                .until(testReceiver.counter::get, equalTo(1));

        Awaitility.await().atMost(10, TimeUnit.SECONDS).pollDelay(100, TimeUnit.MILLISECONDS)
                .until(testConflictReceiver.counter::get, equalTo(1));

        verify(exactly(1), getRequestedFor(urlEqualTo("/api/kafkaidempotentconsumerdemo/" + key)));

        assertThat(testReceiver.counter.get(), equalTo(1));
        assertThat(testConflictReceiver.counter.get(), equalTo(1));

        assertThat(businessRepository.count(), equalTo(1L));
        assertThat(outboxEventRepository.count(), equalTo(1L));
        OutboxEntity outboxEntity = outboxEventRepository.findAll().get(0);
        assertThat(outboxEntity.getPayload(), equalTo(TestEventData.INBOUND_DATA));
    }
}
