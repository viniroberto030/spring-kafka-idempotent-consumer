package demo.kafka.domain;

import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="TB_OUTBOX")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OutboxEntity {

    public static final int VARCHAR_MAX_LENGTH = 4096;

    @Id
    private UUID id;

    @Column(nullable = false, length = VARCHAR_MAX_LENGTH)
    private String payload;

    @Column(nullable = false)
    private long timestamp;

    @Column(nullable = false)
    private String destination;

    private String version;
}
