package demo.kafka.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity(name="TB_BUSINESS")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BusinessEntity {
    @Id
    private UUID id;

    @Column(nullable = false)
    private String campo;
}
