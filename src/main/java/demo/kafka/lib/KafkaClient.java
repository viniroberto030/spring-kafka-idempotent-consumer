package demo.kafka.lib;

import java.util.UUID;

import demo.kafka.domain.OutboxEntity;
import demo.kafka.exception.KafkaDemoException;
import demo.kafka.properties.KafkaDemoProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KafkaClient {
    @Autowired
    private KafkaDemoProperties properties;

    @Autowired
    private KafkaTemplate kafkaTemplate;

    public static final String EVENT_ID_HEADER_KEY = "id-transacao";

    public SendResult sendMessage(OutboxEntity responseEvent) {
        try {
            String payload = "eventId: " + UUID.randomUUID() + ", instanceId: "+properties.getInstanceId()+", payload: " + responseEvent.getPayload();
            final ProducerRecord<String, String> record =
                    new ProducerRecord<>(responseEvent.getDestination(), payload);

            return (SendResult) kafkaTemplate.send(record).get();
        } catch (Exception e) {
            throw new KafkaDemoException(e);
        }
    }
}
